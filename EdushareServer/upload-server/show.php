<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" />
<title>Edushare</title>
</head>

<body>
<div align="center">
<?php

// Define the full path to your folder from root
    $path = "./uploads";
  
    // Open the folder
    $dir_handle = @opendir($path) or die("Unable to open $path");
  
  	$count = count(readdir($dir_handle));
  
    // Loop through the files
    while ($file = readdir($dir_handle)) {
  
    	if($file == "." || $file == ".." || $file == "index.php" || $file == ".DS_Store" )
  			continue;
    
    	echo '<video width="250" height="125" autoplay>' .
			'<source src="./uploads/'.$file.'" type="video/mp4"></source>' .
			'</video><br>';
  
    }
    // Close
    closedir($dir_handle);
?>
</div>
</body>

</html>