package com.monzir.edushare;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final int VIDEO_CAPTURE = 101;
	private Uri fileUri;

	int serverResponseCode = 0;
	ProgressDialog dialog = null;

	String upLoadServerUri = null;
	String filePath = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		WebView mWebView = (WebView) findViewById(R.id.webView);
		mWebView.setWebChromeClient(new WebChromeClient());
		mWebView.setWebViewClient(new WebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.loadUrl("http://192.168.0.12/upload-server/show.php");
	}

	public void startRecording(View view)
	{
		Date date = new Date();

		filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + date.toString().replace(" ", "_").replace(":", "_")+".mp4";

		//File mediaFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.mp4");
		File mediaFile = new File(filePath);

		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		fileUri = Uri.fromFile(mediaFile);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, VIDEO_CAPTURE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	    
		if (requestCode == VIDEO_CAPTURE) {
			if (resultCode == RESULT_OK) {
				Toast.makeText(this, "Video has been saved to:\n" + data.getData(), Toast.LENGTH_LONG).show();
				
				new UploadFile().execute();
			}

		} else if (resultCode == RESULT_CANCELED) {
			Toast.makeText(this, "Video recording cancelled.", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Failed to record video", Toast.LENGTH_LONG).show();
		}
	}

	class UploadFile extends AsyncTask<Void, Void, Void>{

		ProgressDialog pDialog = new ProgressDialog(MainActivity.this);

		protected void onPreExecute() {

			pDialog.setMessage("Uploading File. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				FileInputStream fis = new FileInputStream(filePath);
				FileUploader fu = new FileUploader("http://192.168.0.12/upload-server/upload.php","noparamshere", filePath);
				fu.doStart(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(Void result) {

			pDialog.dismiss();
		}
	}
}

